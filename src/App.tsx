import { BluetoothTwoTone, PsychologyTwoTone } from '@mui/icons-material';
import { AppBar, Button, Container, Grid, Toolbar, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { MuseClient } from 'muse-js';
import React, { useState } from 'react';

function App() {
  const muse = new MuseClient()
  const [error, setError] = useState("")
  const [reading, setReading] = useState("")
  const [telemetry, setTelemetry] = useState("")
  const [acceleration, setAcceleration] = useState("")

  const handleConnect = async () => {
    muse.connect().then((response) => {
      setError("Successfully connected")

      muse.start().then((response) => {
        setError("Successfully started")
        muse.eegReadings.subscribe(reading => {
          console.log(reading)
          setReading(JSON.stringify(reading))
        })
        muse.telemetryData.subscribe(telemetry => {
          console.log(telemetry)
          setTelemetry(JSON.stringify(telemetry))
        })
        muse.accelerometerData.subscribe(acceleration => {
          console.log(acceleration)
          setAcceleration(JSON.stringify(acceleration))
        })
      }).catch((err) => {
        setError(err.message)
      })

    }).catch((err) => {
      setError(err.message)
    })
  }

  return (
    <Box sx={{flexGrow: 1}}>
      <AppBar position="static">
        <Toolbar>
          <PsychologyTwoTone fontSize='large' sx={{pr: 1,}} />
          <Typography variant="h6" component="div" sx={{flexGrow: 1}}>SGD Mind Watcher</Typography>
        </Toolbar>
      </AppBar>
      <Container maxWidth={false} sx={{pt: 2,}}>
        <Grid container>
          <Grid item xs={12}>
            <Button onClick={handleConnect} variant="contained" startIcon={<BluetoothTwoTone />}>Connect</Button>
          </Grid>
          <Grid item xs={12}>
            <p>{error}</p>
          </Grid>
          <Grid item xs={12}>
            <p>{reading}</p>
          </Grid>
          <Grid item xs={12}>
            <p>{telemetry}</p>
          </Grid>
          <Grid item xs={12}>
            <p>{acceleration}</p>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}

export default App;
